.PHONY: doc

all: clean sdist

clean:
	@echo Clean Python build dir...
	python2.7 setup.py clean --all
	@echo Clean Python distribution dir...
	@-rm -rf dist
	@-rm -rf *egg-info

sdist:
	@echo Building the distribution package...
	python2.7 setup.py sdist

install:
	@echo Install the package...
	pip install .

uninstall:
	@echo Uninstalling the package...
	pip uninstall -y biopax2cadbiom

dev_install:
	@echo Install the package for developers...
	@# Replacement for python setup.py develop which doesn't support extra_require keyword.
	@# Install a project in editable mode.
	pip install -e .[dev]

unit_tests:
	@echo Launch unit tests
	python2.7 setup.py test

upload: clean sdist
	python setup.py bdist_wheel
	twine upload dist/* -r pypitest_inria
	
doc:
	@echo "Make sphinx documentation..."
	$(MAKE) -C doc/ html

t:
	pytest

black:
	/home/Lex/.local/bin/black biopax2cadbiom

p:
	biopax2cadbiom model --graph_uris http://biopax.org/lvl3 http://www.pathwaycommons.org/v9/pid --backup_file pickle.pid --backup_queries
k:
	biopax2cadbiom model --graph_uris http://biopax.org/lvl3 http://www.pathwaycommons.org/v10/kegg --backup_file pickle.kegg --backup_queries
r:
	biopax2cadbiom model --graph_uris http://biopax.org/lvl3 http://www.pathwaycommons.org/v9/reactome --backup_file pickle.reactome --backup_queries
c:
	biopax2cadbiom model --graph_uris http://biopax.org/lvl3 http://acsn.curie.fr/emt_senescence_master
cg:
	biopax2cadbiom model --graph_uris http://biopax.org/lvl3 http://acsn.curie.fr/global --backup_file pickle.curie --backup_queries
ctd:
	biopax2cadbiom model --triplestore http://rdf.pathwaycommons.org/sparql --graph_uris http://pathwaycommons.org --provenanceUri http://pathwaycommons.org/pc12/Provenance_b3c525b2c0027b58914b1a79ecc37320 --backup_file pickle.ctd --backup_queries --limit_sparql_results 5000
m:
	python -m biopax2cadbiom model --graph_uris http://biopax.org/lvl3 http://reactome.org/mycobacterium --full_graph
	cadbiom_cmd model_comp testCases/refs/mycobacterium.bcx output/model.bcx --json --graphs

i:
	#cadbiom_cmd model_info output/model.bcx --graph --json
	cadbiom_cmd model_info --json --csv --genes output/model.bcx

ct:
	# test controls
	biopax2cadbiom model --graph_uris http://biopax.org/lvl3 http://virtualcases.org/test_control_1 --backup_file pickle.ctrl_test --backup_queries

ct2:
	biopax2cadbiom model --graph_uris http://biopax.org/lvl3 http://virtualcases.org/test_control_2 --backup_file pickle.ctrl_test2 --backup_queries --full_graph

SRP9:
	cadbiom_cmd -vv info compute_macs "output/model.bcx" SRP9 --all_macs --steps 20  --output SRP9_new/

compare_tests:
	biopax2cadbiom model --graph_uris http://biopax.org/lvl3 http://reactome.org/homarus
	cadbiom_cmd model_comparison output/model.bcx testCases/refs/homarus.bcx

	biopax2cadbiom model --graph_uris http://biopax.org/lvl3 http://reactome.org/crithidia
	cadbiom_cmd model_comparison output/model.bcx testCases/refs/crithidia.bcx

	biopax2cadbiom model --graph_uris http://biopax.org/lvl3 http://reactome.org/vigna
	cadbiom_cmd model_comparison output/model.bcx testCases/refs/vigna.bcx

	biopax2cadbiom model --graph_uris http://biopax.org/lvl3 http://reactome.org/triticum
	cadbiom_cmd model_comparison output/model.bcx testCases/refs/triticum.bcx

	biopax2cadbiom model --full_graph --graph_uris http://biopax.org/lvl3 http://reactome.org/cavia
	cadbiom_cmd model_comparison output/model.bcx testCases/refs/cavia.bcx

	biopax2cadbiom model --full_graph --graph_uris http://biopax.org/lvl3 http://virtualcases.org/2
	cadbiom_cmd model_comparison output/model.bcx testCases/refs/virtualCase2.bcx

