from python:2.7.13-jessie

RUN apt-get update && apt-get install -y \
        python2.7-dev \
        libxml2-dev \
        libxslt1-dev \
        pkg-config \
	build-essential \
	python-pip

COPY . /biopax2cadbiom
WORKDIR /biopax2cadbiom

RUN pip install -r requirements.txt
RUN pip install .

WORKDIR /root
RUn rm -rf /biopax2cadbiom

CMD ["bash"]
