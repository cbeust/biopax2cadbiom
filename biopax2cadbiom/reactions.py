# -*- coding: utf-8 -*-
# MIT License
#
# Copyright (c) 2017 IRISA, Pierre Vignet
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Contributor(s): Pierre Vignet
"""Create reactions


Debug examples:

.. code-block:: python

    # Extract primitives (classes) from a set of entities
    from biopax2cadbiom.reactions import get_classes_from_reactants
    ret = get_classes_from_reactants(
     ["http://pathwaycommons.org/pc2/#Complex_9b6a5665172e4e0ba45a78a54dc9d784"],
     dictPhysicalEntity
    )

    # Get the duplicated reactions from one reaction
    from biopax2cadbiom.reactions import update_reactions
    new_reactions = update_reactions(
     dictReaction["http://identifiers.org/reactome/R-HSA-8867756"],
     dictPhysicalEntity,
     merged_entities_mapping
    )

    # Get banned couples of reagents/products for a reaction
    from biopax2cadbiom.reactions import get_banned_reagents_products_couples
    banned_reactants_couples = get_banned_reagents_products_couples(
     dictReaction["http://pathwaycommons.org/pc2/#ComplexAssembly_6b5f430872bbd85f99432c5d3ced7e45"],
     dictPhysicalEntity,
     merged_entities_mapping
    )

"""
# Standard imports
from __future__ import unicode_literals
from __future__ import print_function
import itertools as it
from copy import deepcopy
from collections import OrderedDict

# Custom imports
from biopax2cadbiom.tools import parse_uri
import biopax2cadbiom.commons as cm

LOGGER = cm.logger()


def get_uris_names_from_components(entity_uri, entity, dictPhysicalEntity):
    """Return list of tuples with URIs associated to a cadbiom name.

    Each entity is decompiled

    .. todo:: cadbiom_names devrait etre un dictionnaire avec
        en clé les uris des composants et en valeurs les noms associés
        => sans ça, impossible de réassocier les noms aux uris qui en sont à
        l'origine. Ça pourrait servir par ex dans
        get_names_of_missing_physical_entities() pour ne pas risquer d'associer
        un membre à une uri qui n'est pas la sienne (son parent ici)...

    :param entity_uri: Unified uri (not the uri of the entity object which is
        the one before the merge of entities).
    :param entity: An entity from which uris and names are extracted.
    :type entity_uri: <str>
    :type entity: <PhysicalEntity>
    :return: list of tuples ((uris), name)
    :rtype: <list <tuple <tuple, <str>>>
    """
#    print("entity studied in ListOfPossibilitie", entity)
    listOfEquivalentsAndCadbiomName = []

    if entity.flat_components:
        # Complex
        # AB est 1 complexe
        # flatcomponents: [(A1, B), (A2,B)]
        # On en fait 1 structure :
        # AB = [ ((A1,B), "A1B"), ((A2,B), "A2B"))]
        listOfEquivalentsAndCadbiomName = \
            zip(entity.flat_components, entity.cadbiom_names)

    elif entity.membersUsed:
        # Class
        for sub_entity_uri in entity.membersUsed:
            if sub_entity_uri == entity_uri:
                # Deprecated code
                # /!\ An entity can be in membersUsed of itself because
                # it represents all members not used.
                # A = classe avec A1 et A2
                # listOfEquivalentsAndCadbiomName sera:
                # A = [ ((A1), "A1"), ((A2), "A2"))]
                listOfEquivalentsAndCadbiomName.append(
                    (tuple([entity_uri]), entity.cadbiom_name)
                )
                LOGGER.error("CLASS = member; %s", parse_uri(sub_entity_uri))
                raise AssertionError("Deprecated code - Should never be reached")
            else:
                # Member of the class used in at least 1 reaction
                listOfEquivalentsAndCadbiomName += \
                    get_uris_names_from_components(
                        sub_entity_uri,  # Use unified uri
                        dictPhysicalEntity[sub_entity_uri],
                        dictPhysicalEntity
                    )
    else:
        # Basic entity
        listOfEquivalentsAndCadbiomName.append(
            (tuple([entity_uri]), entity.cadbiom_name)
        )
#    print("entity studied in ListOfPossibilitie result (uri, name)", listOfEquivalentsAndCadbiomName)
    return listOfEquivalentsAndCadbiomName


def is_common_entities(entities1, entities2, dictPhysicalEntity):
    """Check common entities or references between 2 sets of entities.

    This function is used to make a transition between 2 sets of entities.
    => Is there any transition between these 2 sets?

    The result is based on the uris or the entityRefs of entities.
    We do not take care of the entityRefs of the components for complexes.

    .. note:: Complexes are not supposed to carry entityRefs:

        Code example::

            for entity in dictPhysicalEntity.itervalues():

                if entity.entityType == "Complex" and entity.entityRef:
                    print(entity.uri)
                    print(entity.name)
                    print(entity.modificationFeatures)
                    #print(entity.location.name)
                    print(entity.members)
                    print(entity.entityRef)
                    print('\n')
                    raw_input('entityRef found in complex')

    :param entities1: List of uris of entities.
    :param entities2: List of uris of entities.
    :param dictPhysicalEntity: Dictionary of biopax physicalEntities,
        created by the function query.get_biopax_physicalentities()
    :type entities1: <list>
    :type entities2: <list>
    :type dictPhysicalEntity: <dict <str>: <PhysicalEntity>>
        keys: uris; values entity objects
    :return: False if one set of entities have no entityRefs, or [if
        entityRefs in the first are not in the second and
        entityRefs in the second are not in the first].
        True otherwhise, or if entities1 is a subset of entities2,
        or if entities2 is a subset of entities1.
    :rtype: <bool>
    """
    entities1 = set(entities1)
    entities2 = set(entities2)
    LOGGER.warning("is_common_entities:: Sets of entities %s vs %s", entities1, entities2)

    # Check if entities in 1 are in 2, or entities in 2 are in 1
    if entities1 & entities2:
        return True

    # Get all entityRefs from entities1 (if they are present in these entities)
    g = (dictPhysicalEntity[entity_uri].entityRef for entity_uri in entities1)
    entityRefs1 = {entity_ref for entity_ref in g if entity_ref is not None}

    # Get all entityRefs from entities2 (if they are present in these entities)
    g = (dictPhysicalEntity[entity_uri].entityRef for entity_uri in entities2)
    entityRefs2 = {entity_ref for entity_ref in g if entity_ref is not None}

    LOGGER.debug("is_common_entities:: Sets of references %s vs %s", entities1, entities2)
    # No refs in 1 side => stop
    if not entityRefs1 or not entityRefs2:
        return False
    # Check if entities in 1 are in 2, or entities in 2 are in 1
    return True if entityRefs1 & entityRefs2 else False


def duplicate_complexes(dictPhysicalEntity):
    """Duplicate physical entities that are complexes: 1 new complex per flat component

    The aim is to quickly find these entities when transitions will be computed
    and reactions pruned.
    From now, reactions carry primitive elements like the new complexes
    instead of elements with classes.

    .. Ex: A Complex AB with 4 flat_components due to A, and B classes:

        - Will give 4 **simple complexes** with only 1 element in their
          `flat_components`.
        - All these new complexes are added to dictPhysicalEntity with their
          respective flat_component (a tuple) as keys.

    .. Ex: For VirtualCase4: 6 new simple complexes.

    The unique "uri"/key of a new complex added to dictPhysicalEntity
    (similar to the uri of original entities) is a tuple containing the
    following data: (original_uri, (flat_component))

    .. TODO:: Avoid to use tuples as uri in dictPhysicalEntity
        Search this flat component in the parent object, to retrieve its position
        in flat_components
        => get an id used to compute a simili uri

    .. note:: New physical entities are used in
        :meth:`replace_pruned_complexes_in_reactants`

    .. note:: dictPhysicalEntity is modified here.
    """
    new_physical_entities = dict()
    for physical_entity in dictPhysicalEntity.itervalues():
        # Do not touch complexes with only 1 or 0 flat_component
        # Note: i.e. empty or some class complex (the latter may themselves have
        # components)
        if physical_entity.is_complex and len(physical_entity.flat_components) > 1:

            # Make a new entity for each flat_component
            # - With this flat_component only
            # - Associate to it the corresponding cadbiom_name
            # PS: flat_components and cadbiom_name are in the same order
            for i, flat_component in enumerate(physical_entity.flat_components):
                # Duplication
                new_complex = deepcopy(physical_entity)
                # Only 1 flat_component
                new_complex.flat_components = [flat_component]
                # Only 1 cadbiom_name
                cadbiom_name = new_complex.cadbiom_names[i]
                new_complex.cadbiom_names = [cadbiom_name]
                new_complex.cadbiom_name = cadbiom_name

                # Insert the new complex in new_physical_entities
                # The unique key (similar to the uri of original entities)
                # is a tuple (original_uri, (flat_component))
                # This is done to ensure unicity (flat_component is not sufficient)
                unique_key = (physical_entity.uri, flat_component)

                if flat_component in new_physical_entities:
                    LOGGER.error("Unique key: %s", unique_key)
                    LOGGER.error("Complex already in place: %s", new_physical_entities[unique_key])
                    LOGGER.error("Proposed complex: %s", new_complex)
                    raise AssertionError("duplicate_complexes:: new complex already in new_physical_entities")

                new_physical_entities[unique_key] = new_complex

        # TODO: "1FLAT": ACTIVATE THIS LINE OR THE SIMILAR LINE in replace_and_build()
        # Complex with only 1 flat component (1 member used among the others)
        # Force duplication here but almost useless; we will have the original complex and the same data
        # under another key (the tuple of the unique flat_component)
        # if physical_entity.is_complex and len(physical_entity.flat_components) <= 1:
        #    new_physical_entities[(physical_entity.uri, physical_entity.flat_components[0])] = physical_entity

    # print(new_physical_entities)
    LOGGER.debug("Duplicated complexes: %s", len(new_physical_entities))
    # Merge by mutating dictPhysicalEntity
    dictPhysicalEntity.update(new_physical_entities)


def get_banned_reagents_products_couples(reaction, dictPhysicalEntity, merged_entities_mapping):
    """Get impossible couples of (reagent, product) based on the classes that
    compose them.

    Retourne des couples impossibles de réactants (réactif, produit) sur la base
    des classes impliquées dans leur composition.
    Ne doit retourner les couples bannis que si une correspondance a été
    établie au cours du produit cartésien entre réactifs et produits.

    But : prendre en charge les classes "auto génératrices" qui contiennent
    les memes éléments de part et d'autre: il faut relier les bons éléments
    les uns avec les autres (les réactifs avec leurs produits).
    Cf VirtualCase9b.


    Example: Banned couples for the test case Cavia::

        {
            ("Protein3", "Protein5"),
            ("Protein2", "Protein6")
        }

    Example: Banned couples for VirtualCase9b::

        {
            ('Protein_B', 'Protein_A_duplicated_76ca1466d87bac8b56c505d005926645'),
            ('Protein_A', 'Protein_B_duplicated_76ca1466d87bac8b56c505d005926645')
        }

    .. note:: Comment utiliser le résultat de cette fonction ?
        - si match: se préoccuper des couples à bannir et invalider les réactions
          qui les contiennent;
        - si pas match: retourne un ensemble vide; pas de couple à bannir


    .. TODO:: Pas de détection si mapping erroné entre un membre de classe
        et complexe un complexe coté de la réaction...
        Vu que le is_common_entities() ne prendra pas en compte le complexe
        (car les complexes n'ont pas d'entityRef)...
        Test unitaire à faire...

    :return: Set of tuples of uris: (reagent_uri, product_uri)
    :rtype: <frozenset <tuple <<str>, <str>>>
    """

    def get_uris_from_components(reactants):
        """Return URIs of components of classes in the given reactants

        Only classes are handled here, and we return their components thanks to
        :meth:`get_uris_names_from_components`.

        Les classes sont décompilées par la suite, mais les recherches
        de réactants ne se font QUE sur les classes.

        Example for VirtualCase9b::

            reactants: {Protein_Y_class}
            get_uris_names_from_components():
                [(('Protein_B'), 'B'), (('Protein_A'), 'A')]
            return: {('Protein_B'), ('Protein_A')}

            then:
            reactants: {Protein_X_class}
            get_uris_names_from_components():
                [
                 (('Protein_B_duplicated_76ca1466d87bac8b56c505d005926645'), 'B_1act'),
                 (('Protein_A_duplicated_76ca1466d87bac8b56c505d005926645'), 'A_1act')
                ]
            return:
                {
                 ('Protein_B_duplicated_76ca1466d87bac8b56c505d005926645'),
                 ('Protein_A_duplicated_76ca1466d87bac8b56c505d005926645')
                }
        """
        # Get list of uris involved in the given reactants
        uris = list()
        for entity_uri in reactants:
            # Classes are decompiled as much as possible
            entity = dictPhysicalEntity[entity_uri]
            # Classes only
            ## TODO: complex/classes are handled like classes because
            ## of partial work made by developComplexEntiry()
            if not entity.membersUsed:
                continue
            # Decompile the entity
            # Get list of tuples [(uri, name), ...]
            ret = get_uris_names_from_components(
                entity_uri,  # Use unified uri (not the uri of the element before the merge of entities)
                entity,
                dictPhysicalEntity
            )
            # Get uris only
            # Since we use only classes here, each tuple contain only 1 element
            # (for complexes, each tuple contain a flat component with many uris)
            uris += tuple(zip(*ret)[0])

        return frozenset(uris)

    #################################################

    # Recursively get classes from reactants
    left = get_uris_from_components(reaction.leftComponents)
    right = get_uris_from_components(reaction.rightComponents)

    print("list of uri left comp", left)
    print("list of uri right comp", right)

    # Build banned couples
    # Example of tested couples and the result of is_common_entities for VirtualCase9:
    # ('Protein_B') vs ('Protein_B_duplicated_76ca1466d87bac8b56c505d005926645'): True
    # ('Protein_B') vs ('Protein_A_duplicated_76ca1466d87bac8b56c505d005926645'): False
    # ('Protein_A') vs ('Protein_B_duplicated_76ca1466d87bac8b56c505d005926645'): False
    # ('Protein_A') vs ('Protein_A_duplicated_76ca1466d87bac8b56c505d005926645'): True
    match_found = False
    banned_couples = list()
    for reagent, product in it.product(left, right):

        # Is any ref in common between the current reagent ant the current product ?
        common_entities = is_common_entities(reagent, product, dictPhysicalEntity)
        LOGGER.debug(
            "banned_reagents_products_couples:: Tested couple: %s vs %s: %s",
            parse_uri(reagent),
            parse_uri(product),
            common_entities
        )

        if common_entities:
            match_found = True
        else:
            if len(reagent) == 1:
                # Simple entity
                reagent = reagent[0]
            if len(product) == 1:
                # Simple entity
                product = product[0]
            # If len != 1: flat_component of a Complex entity
            banned_couples.append((reagent, product))

    # Return couples only if at least 1 common reactant has been found
    # If not: all reactants are distinct => no couple to ban
    #
    # Pas de correspondance établie au cours du produit cartésien
    # => pas de couple à bannir
    if match_found:
        return frozenset(banned_couples)
    return frozenset()


def replace_and_build(dictPhysicalEntity, reac, old_uri, new_uri):
    """Duplicate the given reaction and replace in it the old component uri by
    the new one

    .. todo:: Toutes les classes (directes) sont théoriquement dans l'attribut
        `flat_components_primitives` de chaque complexe.
        Mais les classes imbriquées ne sont pas encore gérées à l'heure actuelle.

    :param old_uri: Uri of a class to be replaced by new_uri.
    :param new_uri: Replacement uri; uri of a member of the class with old_uri.
    :type old_uri: <str>
    :type new_uri: <str>
    :return: The new reaction based on the given one.
    :rtype: <reaction>
    """
    new_reaction = deepcopy(reac)
    reagents = new_reaction.leftComponents
    products = new_reaction.rightComponents
    # Replace in reagents
    if old_uri in reagents:
        reagents.remove(old_uri)
        reagents.add(new_uri)
    # Replace in products
    if old_uri in products:
        products.remove(old_uri)
        products.add(new_uri)

    # complexes attribute of Reaction: <dict>
    # keys: complexes uris (uniq)
    # values: flat_component under construction be iterative replacements
    # of all uris of classes by their members (one by one).

    # flat_components_primitives contains all primitive components of the complex,
    # => search old_uri in these primitives
    # TODO: assert that all classes and subclasses are in flat_components_primitives
    for reactant_uri in it.chain(reagents, products):
        reactant = dictPhysicalEntity[reactant_uri]

        if not reactant.is_complex or (
            reactant.is_complex and old_uri not in reactant.flat_components_primitives):
            # Not complex => do nothing
            continue

        # old_uri is in flat_components_primitives of the current reactant
        print("search %s in complex %s" % (parse_uri(old_uri), parse_uri(reactant_uri)))
        if reactant_uri not in new_reaction.complexes:
            # The current complex is not processed

            # TODO "1FLAT": ACTIVATE THIS LINE OR THE SIMILAR LINE in duplicate_complexes()
            # PS: empty flat_components is not possible here (we iterate on flat_components_primitives)
            if len(reactant.flat_components) <= 1:
                # Keep the original complex in the reactants
                # There is only 1 flat component; when we make the transitions
                # there is no problem to use this uri
                # => bypass replace_pruned_complexes_in_reactants()
                continue

            # Build pre-flat_component
            new_reaction.complexes[reactant_uri] = \
                [new_uri if flat == old_uri else flat for flat in reactant.flat_components_primitives]

        else:
            # The current complex is already in the dict
            # => replace old_uri by new_uri (at the same position)
            # PS: the position is important becaus it is a part of the
            # signature of the flat_component in dictPhysicalEntity
            print("complex: remove %s from %s" % (old_uri, new_reaction.complexes[reactant_uri]))
            # Prune pre-flat_component
            new_reaction.complexes[reactant_uri] = \
                [new_uri if flat == old_uri else flat for flat in new_reaction.complexes[reactant_uri]]

    return new_reaction


def get_classes_from_reactants(primitives, dictPhysicalEntity):
    """Recursively get classes from the given reactants

    :param primitives: Set of uris of components
    :type primitives: <set>
    :return: A generator of primitives (classes)
    :rtype: <generator <uri>>
    """

    for element_uri in primitives:
        element = dictPhysicalEntity[element_uri]
        LOGGER.warning("element tested for primitives %s, is class ? %s", parse_uri(element_uri), element.is_class)

        if element.is_class:
            yield element_uri

        if element.is_complex:
            # TODO use flat_components_primitives here
            # For standard complexes
            for element_uri in get_classes_from_reactants(element.components_uris, dictPhysicalEntity):
                yield element_uri
            # For complexes/classes
            for element_uri in get_classes_from_reactants(element.membersUsed, dictPhysicalEntity):
                yield element_uri


def update_reactions(original_reaction, dictPhysicalEntity, merged_entities_mapping):
    """

    Generally speaking, replacing the classes involved in the participants
    (such as reactants themselves or components of complexes) of a reaction
    poses major problems.

    Most of the processing performed here consists of decompiling the classes
    to regenerate the data as close as possible to the underlying biological
    information. **Indeed, reactions with reactants which do not contain classes
    are not affected.**

    For example, we must ensure that when a class is replaced by its members,
    the (biological) meaning of the reaction is unchanged (a reagent component
    must not give a radically different product, even though it belongs to the
    same parent class).

    Therefore we have chosen a greedy but simple approach which consists in
    generating all possible combinations of reactions; and then to prune them
    with the help of reagents/products pairs of unauthorized class members.

    VF:
    Nous devons par exemple nous assurer que suite à un remplacement d'une classe
    par ses membres, le sens (biologique) de la réaction soit inchangé
    (un composant réactif ne doit pas donner un produit radicalement différent,
    bien qu'ayant appartenu à la même classe mère).

    Par conséquent nous avons choisi une approche gloutonne mais simple qui
    consiste à générer toutes les combinaisons possibles de réactions;
    puis à les élager en se basant sur des couples réactifs/produits non
    autorisés des membres des classes impliquées.

    Tasks performed:

        - Generate banned couples of reagents/products based on the classes that
          compose them.
          See :meth:`get_banned_reagents_products_couples`.
        - Recursively get and ordered set of classes from the reactants.
          The order is mandatory to make replacements in components of complex
          (complicated) entities such as Complexes and/or nested classes or
          Complexes.
        - Build a new reaction for each member of the identified classes.
        - Replace all occurrences of the URI of the class and replace it in
          the reactants and their components.
        - Check rebuilt flat_components for Complexes


    :param original_reaction: BioPAX Reaction that will be duplicated following
        the processing of classes.
    :param merged_entities_mapping: Dictionary of canonical uris as keys,
        and lists of non-canonical linked uris as values.
    :type original_reaction: <Reaction>
    :type merged_entities_mapping: <dict <list>>
    :return: Dict of new reactions for the given reaction.
        This dict is ready to be merged with a global dict of reactions.
        Keys: uris; Values: Reactions objects.
    :rtype: <dict <str>:<Reaction>>
    """
    # Get standard primitives (all reactants)
    primitives = original_reaction.leftComponents | original_reaction.rightComponents

    # /!\ Recursively get classes from reactants
    # Primitives are composed of classes only!
    # They are ordered according to the recursive decompilation order made by
    # get_classes_from_reactants()
    # - Order of replacements:
    # We MUST keep this order but for efficiency reasons we also need a unique
    # set of these classes
    # - Multiple replacements:
    # A second level of interpretation may require to keep the number of
    # classes; thus, do not use a set; but this case has never been encountered.
    # If a bug should appear, the function check_built_flat_components() would
    # be there to inform that a generated flat component was not found in those
    # expected.
    primitives = get_classes_from_reactants(primitives, dictPhysicalEntity)
    primitives = OrderedDict.fromkeys(primitives).keys()
    LOGGER.info("update_reactions:: set of classes: %s", primitives)

    # Replace classes by their members and duplicate reactions for each member
    duplicated_reactions = [original_reaction]
    reactions_number = 0

    for element_uri in primitives:
        element = dictPhysicalEntity[element_uri]
        element_name = parse_uri(element_uri)

        # This list is updated for each member of the participant classes
        new_reactions = list()

        for reac in duplicated_reactions:

            # print("---\nOLD reac", reac)

            # Note:
            # uris of elements in reactions and members of classes are still
            # uris before the merge (non-canonical uris).
            # These different uris point to the same entity in
            # dictPhysicalEntity. Pay attention with this mapping.
            for member_uri in element.membersUsed:
                reactions_number += 1

                # Replace element_uri (class) by member_uri (member):
                # - Duplicate the given reaction,
                # - Replace the old component uri by the new one,
                # - Replace the old uri in 'flat_components_primitives' of complexes
                # in order to setup the 'complexes' attribute of the reaction.
                LOGGER.debug(
                    "update_reactions:: replace '%s' by '%s'",
                    element_name, parse_uri(member_uri)
                )
                new_reaction = replace_and_build(
                    dictPhysicalEntity,
                    reac,
                    element_uri,
                    member_uri
                )
                new_reactions.append(new_reaction)

            duplicated_reactions = new_reactions


    if reactions_number == 0:
        # LOGGER.debug("update_reactions:: Original reaction is untouched")
        assert len(duplicated_reactions) == 1
        # Return it now
        return {original_reaction.uri: original_reaction}


    LOGGER.info(
        "update_reactions:: Original reaction <%s> has been duplicated %s times",
        original_reaction.uri,
        len(duplicated_reactions),
    )
    # print(duplicated_reactions)

    # Check that the rebuilt flat_component of complexes
    # made by replace_and_build() is ok.
    check_built_flat_components(dictPhysicalEntity, duplicated_reactions)

    # Setup last reactions attributes and replace complexes by their
    # flat_components in reactants.
    finish_duplication(duplicated_reactions)

    # Prune reactions
    # Get banned couples of reagents/products
    # This is used to prune duplicated reactions in prune_reactions()
    banned_reactants_couples = get_banned_reagents_products_couples(
        original_reaction,
        dictPhysicalEntity,
        merged_entities_mapping
    )

    duplicated_reactions = {
        reac.uri: reac
        for reac in prune_reactions(duplicated_reactions, banned_reactants_couples)
    }

    LOGGER.debug(
        "update_reactions:: Number of duplicated reactions AFTER pruning: %s",
        len(duplicated_reactions)
    )

    # Ready to build transitions with build_transitions()
    return duplicated_reactions


################################################################################

def check_built_flat_components(dictPhysicalEntity, duplicated_reactions):
    """Check that the rebuilt `flat_component` made by :meth:`replace_and_build` is ok

    Values of the attribute `reac_complexes` should all be in `flat_components`.

    :raises AssertionError: If spurious built flat_component have not been found
        in the expected ones.

    :param duplicated_reactions: List of proposed reactions after the
        decompilatin of the classes.
    :type duplicated_reactions: <list <Reaction>>
    """
    for reaction in duplicated_reactions:
        for complex_uri, flat_component in reaction.complexes.iteritems():

            new_flat_component = tuple(flat_component)
            if new_flat_component in dictPhysicalEntity[complex_uri].flat_components:
                continue
            # Values of reac_complexes should be contained in flat_components.
            # Here is the only check that the rebuilt flat_component made by
            # replace_and_build() is ok.
            LOGGER.error("check_built_flat_components:: Spurious built flat_component for <%s>!", complex_uri)
            LOGGER.error("Search new flat: %s", new_flat_component)
            LOGGER.error("... in expected flats: %s", dictPhysicalEntity[complex_uri].flat_components)
            LOGGER.error(dictPhysicalEntity[complex_uri])
            LOGGER.error("reaction: %s", reaction)
            raise AssertionError


def finish_duplication(duplicated_reactions):
    """Replace complexes by their flat_components in reactants.

    Replace the uri of complexes in reactants by new signatures of pruned
    complexes. See :meth:`replace_pruned_complexes_in_reactants`.

    :param duplicated_reactions: List of proposed reactions after the
        decompilatin of the classes.
    :type duplicated_reactions: <list <Reaction>>
    """
    for reac in duplicated_reactions:

        if not reac.complexes:
            continue
        # Replace pruned complexes in reactants...

        # LOGGER.debug("reac:", reac.short_uri)
        # LOGGER.debug("complexes attr:", pprint.pformat(reac.complexes, 2))
        # LOGGER.debug("old reagents:", parse_uri(reac.leftComponents))
        # LOGGER.debug("old products:", parse_uri(reac.rightComponents))

        complexes_uris = reac.complexes.viewkeys() # set behaviour
        # ... in reagents
        # LOGGER.debug("... in reagents")
        reagents_complexes_uris = complexes_uris & reac.leftComponents
        reac.leftComponents = replace_pruned_complexes_in_reactants(
            reac.complexes, reagents_complexes_uris, reac.leftComponents
        )
        # ... in products
        # LOGGER.debug("... in products")
        products_complexes_uris = complexes_uris & reac.rightComponents
        reac.rightComponents = replace_pruned_complexes_in_reactants(
            reac.complexes, products_complexes_uris, reac.rightComponents
        )

        # LOGGER.debug("new reagents:", parse_uri(reac.leftComponents))
        # LOGGER.debug("new products:", parse_uri(reac.rightComponents))
        # print()


def replace_pruned_complexes_in_reactants(reac_complexes, complexes_uris, reactants):
    """Replace given parent complexes by the signature of their flat components
    in the given set of reactants.

    Ex:
    `reac_complexes`: `{'AB': ['AB', ('A1', 'B1')]}`
    `reactants` (left or right components): `{'AB', 'X', 'Y'}`

    We replace `'AB'` in reactants by the uri/signature of its flat component:
    Updated `reactants`: `{('AB', ('A1', 'B1')), 'X', 'Y'}`

    PS: `('AB', ('A1', 'B1'))` is a key of dictPhysicalEntity BTW.
    => see :meth:`duplicate_complexes` for exaplanations about this unique "uri".

    `reactants` is modified in place and can directly be used to update a reaction.

    .. note:: Since the `flat_component` corresponding to the values of
        `reac_complexes` is built in :meth:`replace_and_build`, **we assume**
        that its length (number of uris) must be the same as any component in
        `flat_components`.

    :param reac_complexes: Dictionary of flat_components as values
        and old complexes uris as keys.
    :param complexes_uris: Set of complexes uris found in reactants.
        We have to replace these values by the corresponding value in
        `reac_complexes`.
    :param reactants: Set of uris to be updated.
    :type reac_complexes: <dict <str>:<list <str>, <tuple>>>
    :type complexes_uris: <set>
    :type reactants: <set>
    """
    # Remove old uris from reactants
    reactants -= complexes_uris
    # Add the new versions of the old uris
    # (and only them, not all the keys of reac_complexes).
    # See duplicate_complexes() for exaplanations about the unique "uri"
    # for new_flat_components that we are building here.
    new_flat_components = {
        (complex_uri, tuple(reac_complexes[complex_uri]))
        for complex_uri in complexes_uris
    }
    return reactants | new_flat_components


def prune_reactions(duplicated_reactions, banned_reactants_couples):
    """Prune reactions that contain impossible/banned couples (reagent/product)

    Also set unique uri and unique event name to the new accepted reactions.

    .. seealso:: meth:`get_banned_reagents_products_couples` that compute this
        constraint in early steps.

    Example: Banned couples for the test case Cavia::

        {
            ("Protein3", "Protein5"),
            ("Protein2", "Protein6")
        }

    Example: Banned couples for VirtualCase9b::

        {
            ('Protein_B', 'Protein_A_duplicated_76ca1466d87bac8b56c505d005926645'),
            ('Protein_A', 'Protein_B_duplicated_76ca1466d87bac8b56c505d005926645')
        }

    Example of pruning reactions containing complexes composed of classes:

        See VirtualCase4

    :param duplicated_reactions: List of proposed reactions after the
        decompilatin of the classes.
    :param banned_reactants_couples: Set of banned couples of reagents/products.
        May be empty.
    :type banned_reactants_couples: <set>
    :type duplicated_reactions: <list <Reaction>>
    """
    # Unique naming of reactions/events by numbering
    duplicate_number = 0 if len(duplicated_reactions) == 1 else 1

    for reac in duplicated_reactions:
        banned_couple_found = False

        for banned_reagent, banned_product in banned_reactants_couples:
            # class member vs duplicated complex ???
            # Les complexes ont des clés qui sont des tuples.
            # il faut que l'ensemble des couples bannis renvoie les
            # flats components (tuples) pour les complexes à ne pas associer
            # aux divers membres des classes décompilées.
            # 1 réaction doit etre bannie ssi le complexe utilisé est en conflit
            # avec un autre élément.
            # Les complexes possédant à l'origine des flats components à cause
            # de classes internes sont seuls dans leurs réactions,
            # après la duplication des réactions pour cause de complexes.
            # En effet, les complexes simples n'aboutissent pas à des duplications
            # de réactions.
            # La détection des banned_reactants_couples doit donc se faire:
            # - juste après la duplication des réactions,
            # - elle-meme après le developpement des complexes.


            # Ex de leftcomponents avec remplacement de complexe: la recherche des couples bannis ne touche
            # pas le tuple interne
#                  'leftComponents': set([ ( u'http://virtualcases.org/14#Complex_AB',
#                            ( u'http://virtualcases.org/14#Protein_A2',
#                              u'http://virtualcases.org/14#Protein_B'))]),
            if banned_reagent in reac.leftComponents and banned_product in reac.rightComponents:
                LOGGER.debug(
                    "prune_reactions:: discard reaction: %s\n"
                    "because of (%s, %s)",
                    reac,
                    parse_uri(banned_reagent), parse_uri(banned_product)
                )
                banned_couple_found = True
                # Do not test this reaction anymore
                break

        if not banned_couple_found or not banned_reactants_couples:
            # Yield the reaction if there is no unwanted couples have not been found
            # or if there is no banned_reactants_couples
            if duplicate_number > 0:
                # Set unique uri to new reactions
                # PS: Do not duplicate the uri if the original reaction is
                # replaced by only one
                suffix = '_' + str(duplicate_number)
                reac.uri += suffix
                reac.event += suffix
                duplicate_number += 1

            yield reac
