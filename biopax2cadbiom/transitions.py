# -*- coding: utf-8 -*-
# MIT License
#
# Copyright (c) 2017 IRISA, Pierre Vignet
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Contributor(s): Pierre Vignet
"""Build transitions"""
# Standard imports
import itertools as it
from logging import DEBUG

# Custom imports
import sympy
import biopax2cadbiom.commons as cm

LOGGER = cm.logger()


def build_transitions(dictPhysicalEntity, dictTransition, reactions):
    """
    Build transitions
    - pour toute réaction r de R
        - récupérer les réactifs
        - faire le produit cartésien entre les réactifs et les produits
        - pour chaque couple réactif/produit:
            - faire une transition
            - construire la condition
                - prendre la condition pré-existante de la réaction
                - y ajouter via un AND l'ensemble des réactifs de la réaction moins
                le réactif dans la transition actuelle.
            - Ajouter la transition et sa condition au dictionnaire global
            DictTransition
    """

    def update_subtransitions(reac_uri, left_entity, right_entity, event, cond):
        """.. todo: Move this function and reuse it elsewhere.
        """
        dictTransition[(left_entity, right_entity)].append(
            {
                'event': event,
                'reaction': reac_uri,
                'sympyCond': cond,
            }
        )

    if LOGGER.getEffectiveLevel() == DEBUG:
        # Dump reactions
        with open("reac.txt", "a") as f_d:
            [f_d.write(str(reac)) for reac in reactions]

    # TODO: itérer une première fois pour récupérer les noms des entités
    # key: uri, value: nom
    for reac in reactions:
        reagents_names = {dictPhysicalEntity[uri].cadbiom_name for uri in reac.leftComponents}
        products_names = {dictPhysicalEntity[uri].cadbiom_name for uri in reac.rightComponents}

        # Cartesian product of reagents with products
        for reagent_name, product_name in it.product(reagents_names, products_names):
            # print(parse_uri(reagent_uri), parse_uri(product_uri))
            # print("transition:", reagent_name, product_name)

            condition = reac.cadbiomSympyCond

            # Build the condition of the current transition
            # We remove the current reagent_name from the condition
            # and keep all other reagents
            for remaining_reagent in reagents_names - set([reagent_name]):
                reagent_symbol = sympy.Symbol(remaining_reagent)
                condition = sympy.And(condition, reagent_symbol)

            update_subtransitions(reac.uri, reagent_name, product_name, reac.event, condition)

    # print(pprint.pformat(dict(dictTransition), 2))
